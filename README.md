# moulage en béton

## expérimentations

### Béton - test 1

<img src="https://framagit.org/MrBidouille/moulages-en-beton/raw/master/Photos/Essai%201JPG%20(1).JPG" alt="drawing" width="600">
<img src="https://framagit.org/MrBidouille/moulages-en-beton/raw/master/Photos/Essai%201JPG%20(6).JPG" alt="drawing" width="600">

**composants :**
* Ciment de base (CEM2, 32,5R)
* Sable fin "Sable à maçonner" granulométrie : 0.1

**dosage :**
* 1 volume d'eau
* 2 volumes de ciment
* 3 volume de sable

Conclusions :
Le moulage s'effrite aux angles et se fends en deux. Pas top. On a mis trop d'eau.

### Béton - test 2

<img src="https://framagit.org/MrBidouille/moulages-en-beton/raw/master/Photos/Essai%202.jpg" alt="drawing" width="600">

**composants :**
* Ciment de base (CEM2, 32,5R)
* Sable fin "Sable à maçonner" granulométrie : 0/1

**dosage :**
* 1 volume d'eau
* 1 volumes de ciment
* 1 volume de sable

**sechage :**
* Sur radiateur

Conclusions :
* Encore trop d'eau, et le moulage s'effrite encore plus. 
* Apres discussion avec des habitués du béton, notre sable est beaucoup trop fin. Il faudrait partir sur du 0/4 jusqu'à 0/6, plus fait pour des bétons conventionnels.
* Il a séché trop vite, il faut le mettre dans un endroit plus frais et plus humide

### Béton - test 3

<img src="https://framagit.org/MrBidouille/moulages-en-beton/raw/master/Photos/Essai%203%20(2).jpg" alt="drawing" width="600">
<img src="https://framagit.org/MrBidouille/moulages-en-beton/raw/master/Photos/Essai%203%20(1).jpg" alt="drawing" width="600">

Vu que notre sable était trop fin (il sera utilisé plus tard pour faire du sable vert pour mouler de l'aluminium), on a tenté un test avec ce qu'on avais sur la main pour voir si la granulométrie joue beaucoup.

**composants :**
* Ciment de base (CEM2, 32,5R)
* Gravier de bourrin (terrain de boules à coté du fablab) disons du 4/14 un truc comme ça. On ne sais pas si c'est "que" du gravier.

**dosage :**
* 0.5 volume d'eau
* 0.75 volumes de ciment
* 1 volume de sable

**sechage :**
* Température ambiante dans l'atelier (hiver).

Conclusions :
Beaucoup mieux. Surface lisse qui ne s'effrite pas. Le tout à l'air beaucoup plus solide. La seichage est aussi plus long.

### Béton - test 4 

Test avec sable de 0/4 et fibre pour béton (pour chapes, etc)

**composants :**
* Ciment de base (CEM2 32,5R)
* Sable 0/4 de base pour faire de la maçonnerie
* Une pincée de fibres

**dosage :**
* Pour test 4.1 :
    * 0.5 volume d'eau
    * 0.75 volumes de ciment
    * 1 volume de sable
* Pour test 4.2 :
    * 0.5 volume d'eau
    * 0.75 volumes de ciment
    * 1 volume de sable
    * 1 pincée de fibres

**sechage :**
* Température ambiante dans l'atelier (hiver).

Conclusions :

### Moulage - test 1

* Forme : sphérique
* Fabrication du moule : Impression 3D
* Composition du béton : test 4.2
* Démoulant : Cire de démoulage pour stratifiés polyester
 
***notes***
Premiere tentative de moulage dans un moule en impression 3D. La forme sphérique est enduite de cire de démoulage, et j'ai poncé la surface pour qu'elle soit à peu pres lisse.
Le moule est en deux partie, la jointure n'est pas parfaite.


## TODO :
* Acheter du sable 0/4 **OK**
* Tester avec des proportions différentes, réduire sable et eau. **OK**
* Tester la fibre dans le béton :  https://chateau-thierry.entrepot-du-bricolage.fr/pr-sikacim-fibres-dose-150gr-rsmf0302-423500,42317,76,361,896.htm **OK**
* Tester de la metakaolin (voir https://www.moertelshop.fr/acheter-metamax-metakaolin-blanc-a-bon-prix_2)

## Tests à faire :
* Mélanger des colles (papier pein, colle à bois)
* Tenter un séchage tres long (au froid et au froid + humidité)

## Sites intéressants :

* achats
    * un site spécialisé dans le moulage en béton https://www.moertelshop.fr/acheter-metamax-metakaolin-blanc-a-bon-prix_2
* recettes
    * https://www.makersgallery.com/concrete/howto2.html
    
